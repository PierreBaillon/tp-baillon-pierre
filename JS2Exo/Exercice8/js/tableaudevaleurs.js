$(document).ready(function () {                                             
    $("#total").html(calculTotal());                                        // affiche le total des valeurs
    
    
    $("#BtnAjouter").click(function () {                                    // Bouton Ajouter
        let longueurliste = $("tbody tr").length + 1;                       // variable qui prend la longueur de la liste
        $("tbody").append("<tr><td>" + longueurliste + "</td></tr>");       // ajout d'une ligne et de sa valeur
        $("#total").html(calculTotal());                                    // calcul du total grâce à la fonction
    })


    $("#BtnSupprimer").click(function () {                                  // Bouton Supprimer
        $("tbody tr:last-child").remove();                                  // supprime le dernier élément
        $("#total").html(calculTotal());                                    // calcul de total grâce à la fonction
    })

    
    function calculTotal() {                                                // déclaration de la fonction pour calculer
        let somme = 0;                                                      // somme = le total des valeurs
        $("tbody td").each(function (index) {                               
            somme += parseInt($(this).html());                              // ajoute la valeur de l'élément
        })
        return somme;                                                       // retourne la somme = la valeurs total
    }
});
