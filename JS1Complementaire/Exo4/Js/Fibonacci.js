//Afficher la suite de Fibonnaci 
//auteur : Baillon Pierre

const NB_CHIFFRES = 17;                                                  // NB de chiffre de la suite que l'on veut


for(var I=0;I<=NB_CHIFFRES;I++) {
    document.write("Fibonacci de "+I+"="+Fibonacci(I)+"<BR>");           //affichage des NB_CHIFFRES premiers chiffre de la Suite de Fibonacci
}



function Fibonacci(prmNombre) {
    var somme;                                              // Variable pour stocker la somme du n1 et n2
    if(prmNombre <= 0) return 0;                            // Affichage du nombre 0 = 0
    if(prmNombre == 1) return 1;                            // Affichage du nombre 1 = 1
    var n1 = 0;
    var n2 = 1;
    for(var i=2; i <= prmNombre; i++) {                     // Calcul des nombre de la suite de fibonacci
        somme = n1+n2;
        n1 = n2;
        n2 = somme;
    };
    return n2;
 }    
     