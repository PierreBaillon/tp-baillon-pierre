$(document).ready(function (){

    $("#idSliderPoids").on('input', function () {  
        affIMC(); 
    });

    $("#idSliderTaille").on('input', function () {  
        affIMC(); 
    });

//********************************************************************************* */
//                         Appel de la fonction affIMC
//********************************************************************************* */
    
function affIMC(){
    // Récuparation des valeurs saisie
    let poids = $("#idSliderPoids").val();     
    let taille = $("#idSliderTaille").val();


    // Affichage du Poids sur le Slider
    $("#textPoids").html(poids);
    // Affichage de la Taille sur le Slider
    $("#textTaille").html(taille);

    imc = calculerIMC(poids, taille);   //Appel de la fonction pour calculer l'IMC
    interpret = interpreterIMC(imc);    //Appel de la fonction pour interpreter le résultat de l'IMC
    $("#textIMC").html(imc.toFixed(1) + interpret); //Afficher l'imc dans textIMC
     
}   

//********************************************************************************* */
//                            Fonction calculerIMC
//********************************************************************************* */
function calculerIMC(prmPoids, prmTaille){
    //Retourne l'IMC
    //2 parametre : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'IMC
    return valRetour;
}

//********************************************************************************* */
//                            Fonction interpreterIMC
//********************************************************************************* */
function interpreterIMC(prmValImc) { 
    //Retourne l'interprétation de l'IMC
    //1 parametre : La valeur de l'IMC
    let interpretation = "";
    
    if (prmValImc<16.5) {                               //Si l'IMC inférieur a 16.5
        interpretation = " dénutrition";
    }else if (prmValImc >= 16.5 && prmValImc < 18.5) {  //Si l'IMC entre 16.5 et 18.5
        interpretation = " maigreur";
    }else if (prmValImc >= 18.5 && prmValImc < 25) {    //Si l'IMC entre 18.5 et 25
        interpretation = " corpulence normale"
    }else if (prmValImc >= 25 && prmValImc < 30) {      //Si l'IMC entre 25 et 30
        interpretation = " surpoids";
    }else if (prmValImc >= 30 && prmValImc < 35) {      //Si l'IMC entre 30 et 35
        interpretation = " obésité modérée";
    }else if (prmValImc >= 35 && prmValImc < 40) {      //Si l'IMC entre 35 et 40
        interpretation = " obésité sévère";
    }else if(prmValImc >= 40){                          //Si l'IMC supérieur à 40
        interpretation = " obésité morbide";
    }

    return interpretation; 
}
});
