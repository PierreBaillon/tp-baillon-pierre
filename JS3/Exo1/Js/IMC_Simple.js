$(document).ready(function (){

//********************************************************************************* */
//                         Appel de la fonction affIMC
//********************************************************************************* */
    $("#btnCalculImc").click(affIMC);


function affIMC(){
    let poids = $("#idPoids").val();    // Variable de stockage du poids
    let taille = $("#idTaille").val();  // Variable de stockage de la taille
    let imc = 0;

    //remplace les , par des .
    poids = poids.replace(",", ".");    
    taille = taille.replace(",", "."); 
    
    //Forçage en nombre
    poids = Number(poids);
    taille = Number(taille);

    //Debut si
    if (isNaN(poids) || isNaN(taille)){     //Si le poid ou la taille sont pas des nombres
        $("#textIMC").html("Erreur !");     //Afficher l'erreur
    }else{                                  //Sinon
        imc = calculerIMC(poids, taille);   //Appel de la fonction pour calculer l'IMC
        $("#textIMC").html(imc.toFixed(1)); //Afficher l'imc dans textIMC
    }  
    //Fin si    
}   

//********************************************************************************* */
//                            Fonction calculerIMC
//********************************************************************************* */
function calculerIMC(prmPoids, prmTaille){
    //Retourne l'IMC
    //2 parametre : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'IMC
    return valRetour;
}

});
