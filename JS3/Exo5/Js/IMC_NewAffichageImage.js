$(document).ready(function (){

    $("#idSliderPoids").on('input', function () {  
        affIMC(); 
    });

    $("#idSliderTaille").on('input', function () {  
        affIMC(); 
    });

//********************************************************************************* */
//                         Appel de la fonction affIMC
//********************************************************************************* */
    
function affIMC(){
    // Récuparation des valeurs saisie
    let poids = $("#idSliderPoids").val();     
    let taille = $("#idSliderTaille").val();


    // Affichage du Poids sur le Slider
    $("#textPoids").html(poids);
    // Affichage de la Taille sur le Slider
    $("#textTaille").html(taille);

    imc = calculerIMC(poids, taille);   //Appel de la fonction pour calculer l'IMC
    interpret = interpreterIMC(imc);    //Appel de la fonction pour interpreter le résultat de l'IMC
    $("#textIMC").html(imc.toFixed(1) + interpret); //Afficher l'imc dans textIMC
    $("#balance").html(afficherBalance(imc));   //Appel de la fonction pour afficher la balance 
    $("#silhouette").html(afficherSilhouette(imc)); //Appel fonction de la silhouette
}   

//********************************************************************************* */
//                            Fonction calculerIMC
//********************************************************************************* */
function calculerIMC(prmPoids, prmTaille){
    //Retourne l'IMC
    //2 parametre : le poid et la taille
    let valRetour = prmPoids / ((prmTaille * prmTaille)/10000); //Calcul l'IMC
    return valRetour;
}

//********************************************************************************* */
//                            Fonction interpreterIMC
//********************************************************************************* */
function interpreterIMC(prmValImc) { 
    //Retourne l'interprétation de l'IMC
    //1 parametre : La valeur de l'IMC
    let interpretation = "";
    
    if (prmValImc<16.5) {                               //Si l'IMC inférieur a 16.5
        interpretation = " dénutrition";
    }else if (prmValImc >= 16.5 && prmValImc < 18.5) {  //Si l'IMC entre 16.5 et 18.5
        interpretation = " maigreur";
    }else if (prmValImc >= 18.5 && prmValImc < 25) {    //Si l'IMC entre 18.5 et 25
        interpretation = " corpulence normale"
    }else if (prmValImc >= 25 && prmValImc < 30) {      //Si l'IMC entre 25 et 30
        interpretation = " surpoids";
    }else if (prmValImc >= 30 && prmValImc < 35) {      //Si l'IMC entre 30 et 35
        interpretation = " obésité modérée";
    }else if (prmValImc >= 35 && prmValImc < 40) {      //Si l'IMC entre 35 et 40
        interpretation = " obésité sévère";
    }else if(prmValImc >= 40){                          //Si l'IMC supérieur à 40
        interpretation = " obésité morbide";
    }

    return interpretation; 
}

//********************************************************************************* */
//                            Fonction afficherBalance
//********************************************************************************* */
function afficherBalance(prmValImc) {
    //Affiche la balance avec 1 parametre
    let deplacement = 0;                 //Variable de déplacement 
    if (prmValImc >= 10 && prmValImc <= 45) {
        deplacement = (prmValImc - 10) * 8.571;
    }

    //Décaler l'aiguille en fonction de la valeur de l'IMC
    $("#aiguille").css("left", deplacement + "px");
}

//********************************************************************************* */
//                            Fonction afficherSilhouette
//********************************************************************************* */
function afficherSilhouette(prmValImc) {
    let decalage;
    
    if (prmValImc<16.5) {
        decalage = 0;
    }else if (prmValImc >= 16.5 && prmValImc < 18.5) {
        decalage = -105;
    }else if (prmValImc >= 18.5 && prmValImc < 25) {
        decalage = -210;
    }else if (prmValImc >= 25 && prmValImc < 30) {
        decalage = -315;
    }else if (prmValImc >= 30 && prmValImc < 35) {
        decalage = -420;
    }else if (prmValImc >= 35 ){
        decalage = -525;
    }
    $("#silhouette").css("background-position", decalage);
    }


});
